import java.util.*;
import java.math.*;

public class HashTableSC<T> {

   private LinkedList<T>[] array;
   
   public HashTableSC(int size) {
      array = (LinkedList<T>[]) new LinkedList[size];
      for(int i=0; i<size; i++) {
         array[i] = new LinkedList<T>();
      }
   }
   
   private int hash(T x) {
      
      return Math.abs(x.hashCode() % array.length);
      
   }
   
   public void insert(T data) {
      
      int location = hash(data);
      array[location].push(data);
      
   }
   
   public void delete(T data) {
      
      int location = hash(data);

      array[location].remove(data);

   }
   
   public boolean find(T data) {
      
      int location = hash(data);

      return array[location].contains(data);
      
   }
   
   public boolean isEmpty() {
      
      for(int i=0; i<array.length; i++) {
         if(!array[i].isEmpty())
            return false;
      }
      
      return true;
   
   }
   
   public void print() {
      
      for(int i=0; i<array.length; i++) {
         
         System.out.print(i + ": ");
         
         for(int j=0; j<array[i].size(); j++) {
            
            System.out.print(array[i].get(j) + " ");
            
         }
         
         System.out.println();
         
      }
      
   }
   
   
   
}
